package io.piveau.metrics.cache;

import io.piveau.dcatap.DCATAPUriSchema;
import io.piveau.dcatap.TripleStore;
import io.piveau.metrics.cache.dqv.DqvProvider;
import io.piveau.metrics.cache.dqv.DqvVerticle;
import io.piveau.metrics.cache.dqv.sparql.QueryCollection;
import io.piveau.metrics.cache.persistence.DocumentScope;
import io.piveau.rdf.Piveau;
import io.piveau.vocabularies.vocabulary.PV;
import io.vertx.core.*;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.junit5.Timeout;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.apache.jena.riot.Lang;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.EnumSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@DisplayName("Testing mock dqvProvider metric functions")
@ExtendWith(VertxExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Testcontainers(disabledWithoutDocker = true)
class MetricTest {

    final private Logger log = LoggerFactory.getLogger(getClass());

    private DqvProvider dqvProvider;

    @Container
    static GenericContainer<?> virtuoso = new GenericContainer<>(DockerImageName.parse("openlink/virtuoso-opensource-7:7.2.14-alpine"))
            .withExposedPorts(8890)
            .withEnv("DBA_PASSWORD", "test");

    @BeforeAll
    void setup(Vertx vertx, VertxTestContext testContext) {
        DCATAPUriSchema.INSTANCE.setConfig(new JsonObject().put("baseUri", "https://example.eu/"));
        QueryCollection.init(vertx, "queries");

        String tripleStoreAddress = "http://" + virtuoso.getHost() + ":" + virtuoso.getFirstMappedPort();
        JsonObject config = new JsonObject()
                .put("address", tripleStoreAddress)
                .put("password", "test");

        TripleStore tripleStore = new TripleStore(vertx, config);

        List<Future<Void>> loadGraphs = List.of(
                loadGraph(vertx, tripleStore, "https://example.eu/id/catalogue/example-catalogue", "Catalogue-Graph.ttl"),
                loadGraph(vertx, tripleStore, "https://example.eu/set/data/test-dataset-1", "test-dataset-1-graph.ttl"),
                loadGraph(vertx, tripleStore, "https://example.eu/id/metrics/test-dataset-1", "test-dataset-1-metrics-graph.ttl"),
                loadGraph(vertx, tripleStore, "https://example.eu/set/data/test-dataset-2", "test-dataset-2-graph.ttl"),
                loadGraph(vertx, tripleStore, "https://example.eu/id/metrics/test-dataset-2", "test-dataset-2-metrics-graph.ttl")
        );

        Future.all(loadGraphs)
                .compose(v -> vertx.deployVerticle(DqvVerticle.class, new DeploymentOptions().setConfig(config)))
                .onSuccess(id -> {
                    dqvProvider = DqvProvider.createProxy(vertx, DqvProvider.SERVICE_ADDRESS, new DeliveryOptions().setSendTimeout(3000000));
                    testContext.completeNow();
                })
                .onFailure(testContext::failNow);
    }

    @DisplayName("get Scores")
    @ParameterizedTest
    @CsvSource(value= {
            "GLOBAL,1",
            "CATALOGUE,example-catalogue",
            "COUNTRY,GBR" }
    )
    void getScores(String scope, String id, Vertx vertx, VertxTestContext testContext) {
        dqvProvider.getAverageScore(id, DocumentScope.valueOf(scope), PV.scoring.getURI())
                .onSuccess(value -> testContext.verify(() -> {
                    assertNotNull(value);
                    assertEquals(173, value);
                    testContext.completeNow();
                }))
                .onFailure(testContext::failNow);
    }

    @DisplayName("get a Distribution Metric")
    @ParameterizedTest
    @CsvSource(value= {
            "GLOBAL,1",
            "CATALOGUE,example-catalogue",
            "COUNTRY,GBR" }
    )
    void getDistMetric(String scope, String id, Vertx vertx, VertxTestContext testContext) {
        dqvProvider.getFormatAvailability(id, DocumentScope.valueOf(scope), -1L)
                .onSuccess(value -> testContext.verify(() -> {
                    assert (value != null);
                    assert (100 == Math.round(value));
                    testContext.completeNow();
                }))
                .onFailure(testContext::failNow);
    }

    @Test
    @DisplayName("get violations")
    void getViolations(Vertx vertx, VertxTestContext testContext) {
        Future<JsonObject> violationsFuture = dqvProvider.getCatalogueViolations("example-catalogue", 0, 25, "de");
        Future<JsonObject> violationsCountFuture = dqvProvider.getCatalogueViolationsCount("example-catalogue");

        //if futures calls are completed
        Future.all(violationsFuture, violationsCountFuture).onSuccess(compositeFuture -> {
            JsonObject resultViolations = violationsFuture.result();
            JsonObject resultCount = violationsCountFuture.result();
            testContext.verify(() -> {
                assertEquals(15, resultCount.getInteger("count", -1));
                assertEquals(15, resultViolations.getJsonObject("result").getJsonArray("results").size());
            });
            testContext.completeNow();
        }).onFailure(testContext::failNow);
    }

    @Test
    @DisplayName("get reachability")
    void getReachability(Vertx vertx, VertxTestContext testContext) {
        dqvProvider.getDistributionReachabilityDetails("example-catalogue", 0, 4, "en")
                .onSuccess(result -> {
                    log.debug(result.encodePrettily());
                    testContext.verify(() -> {
                        assertEquals(1, result.getInteger("count", -1));
                        assertEquals(1, result.getJsonArray("results").size());
                        assertEquals(404, result.getJsonArray("results").getJsonObject(0).getInteger("accessUrlStatusCode"));
                        assertEquals("Test Dataset", result.getJsonArray("results").getJsonObject(0).getString("title"));
                    });
                    testContext.completeNow();
                })
                .onFailure(testContext::failNow);
    }

    @Test
    @DisplayName("Get reachability language de")
    void getReachabilityLangDe(Vertx vertx, VertxTestContext testContext) {
        //get results with an german title
        dqvProvider.getDistributionReachabilityDetails("example-catalogue", 0, 4, "de")
                .onSuccess(result -> {
                    testContext.verify(() -> {
                        assertEquals(1, result.getInteger("count", -1));
                        assertEquals(1, result.getJsonArray("results").size());
                        assertEquals("Title in german", result.getJsonArray("results").getJsonObject(0).getString("title"));
                        testContext.completeNow();
                    });
                })
                .onFailure(testContext::failNow);
    }

    @Test
    @DisplayName("Get reachability language es")
    void getReachabilityLangEs(Vertx vertx, VertxTestContext testContext) {
        //try to get results with italian title, but this language does not exist
        dqvProvider.getDistributionReachabilityDetails("example-catalogue", 0, 4, "es")
                .onSuccess(result -> {
                    testContext.verify(() -> {
                        assertEquals(1, result.getInteger("count", -1));
                        assertEquals(1, result.getJsonArray("results").size());
                        assertEquals("Title in spanish", result.getJsonArray("results").getJsonObject(0).getString("title"));
                    });
                    testContext.completeNow();
                })
                .onFailure(testContext::failNow);
    }

    @Test
    @DisplayName("Get catalogue infos")
    void getCatalogueInfos(Vertx vertx, VertxTestContext testContext) {
        dqvProvider.listCatalogues()
                .onSuccess(result -> {
                    log.debug(result.toString());
                    testContext.completeNow();
                })
                .onFailure(testContext::failNow);
    }

    private Future<Void> loadGraph(Vertx vertx, TripleStore tripleStore, String graphName, String resource) {
        return vertx.fileSystem().readFile(resource)
                .map(buffer -> Piveau.toModel(buffer.getBytes(), Lang.TURTLE))
                .compose(model -> tripleStore.setGraph(graphName, model).mapEmpty());
    }

}