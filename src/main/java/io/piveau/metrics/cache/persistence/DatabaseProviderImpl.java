package io.piveau.metrics.cache.persistence;

import io.piveau.log.PiveauLogger;
import io.piveau.metrics.cache.dqv.Dimension;
import io.piveau.metrics.cache.dqv.DqvProvider;
import io.piveau.metrics.cache.dqv.sparql.util.PercentageMath;
import io.piveau.metrics.cache.dqv.sparql.util.SparqlHelper;
import io.piveau.metrics.cache.persistence.util.Aggregation;
import io.piveau.utils.PiveauContext;
import io.piveau.vocabularies.vocabulary.PV;
import io.vertx.core.*;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.*;

import static io.piveau.metrics.cache.ApplicationConfig.*;

import static io.piveau.metrics.cache.dqv.sparql.util.SparqlHelper.getCountries;

public class DatabaseProviderImpl implements DatabaseProvider {

    private static final Logger log = LoggerFactory.getLogger(DatabaseProviderImpl.class);

    private final DqvProvider dqvProvider;
    private final MongoClient dbClient;

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    private final PiveauLogger piveauLog = new PiveauContext("metrics-cache", "cache").log();

    private boolean refreshRunning = false;

    /**
     * Default constructor
     *
     * @param vertx        the vert.x object
     * @param readyHandler called, when everything is set up, fails, when no db connection can be established
     */
    public DatabaseProviderImpl(Vertx vertx, Handler<AsyncResult<DatabaseProvider>> readyHandler) {

        //establish connection to the dqv verticle
        DeliveryOptions options = new DeliveryOptions().setSendTimeout(3000000);
        dqvProvider = DqvProvider.createProxy(vertx, DqvProvider.SERVICE_ADDRESS, options);

        //establish db connection
        JsonObject env = vertx.getOrCreateContext().config();

        JsonObject config = new JsonObject()
                .put("connection_string", env.getString(ENV_MONGODB_CONNECTION, DEFAULT_MONGODB_CONNECTION))
//                .put("connectTimeoutMS", 30000)
//                .put("socketTimeoutMS", 10000)
//                .put("serverSelectionTimeoutMS", 1000)
//                .put("keepAlive", true)
                .put("db_name", env.getString(ENV_MONGODB_DB_NAME, DEFAULT_MONGODB_DB_NAME));

        log.debug("MongoDB config: {}", config);
        dbClient = MongoClient.createShared(vertx, config);

        readyHandler.handle(Future.succeededFuture(this));
    }

    private final List<String> filterValues = Arrays.asList(
            "findability",
            "accessibility",
            "interoperability",
            "reusability",
            "contextuality",
            "score",
            "info"
    );

    /**
     * Get a single document
     *
     * @param documentScope the collection the document is in
     * @param id            the document ID, that is, the catalogue ID, a three letter abbreviation for a country or "global"
     * @param filter        list of dimensions that should should be returned in the document
     * @param resultHandler called with an result object
     */
    @Override
    public void getDocument(DocumentScope documentScope, String id, List<String> filter, Handler<AsyncResult<JsonObject>> resultHandler) {
        if (filter.isEmpty()) {
            filter.addAll(filterValues);
        } else {
            filter.retainAll(filterValues);
        }

        new Aggregation(id, dbClient, documentScope, filter).aggregateCurrent(resultHandler);
    }

    @Override
    public void getDocumentList(DocumentScope documentScope, List<String> filter, Handler<AsyncResult<JsonObject>> resultHandler) {
        if (filter.isEmpty()) {
            filter.addAll(filterValues);
        } else {
            filter.retainAll(filterValues);
        }
        new Aggregation(dbClient, documentScope, filter).aggregateCurrent(resultHandler);
    }

    @Override
    public void getHistory(DocumentScope documentScope, String id, List<String> filter, String resolution, String startDate, String endDate, Handler<AsyncResult<JsonObject>> resultHandler) {
        if (filter.isEmpty()) {
            filter.addAll(filterValues);
        } else {
            filter.retainAll(filterValues);
        }

        new Aggregation(id, dbClient, documentScope, filter).aggregateHistoric(startDate, endDate, resolution, resultHandler);
    }


    @Override
    public void getHistoryList(DocumentScope documentScope, List<String> filter, String resolution, String startDate, String endDate, Handler<AsyncResult<JsonObject>> resultHandler) {
        if (filter.isEmpty()) {
            filter.addAll(filterValues);
        } else {
            filter.retainAll(filterValues);
        }

        new Aggregation(dbClient, documentScope, filter).aggregateHistoric(startDate, endDate, resolution, resultHandler);
    }

    @Override
    public void deleteDocument(DocumentScope documentScope, String id, Handler<AsyncResult<String>> resultHandler) {
        JsonObject filter = new JsonObject().put("_id", id);
        dbClient.removeDocument(documentScope.name(), filter)
                .compose(result -> dbClient.removeDocument(DocumentScope.SCORE.name(), filter))
                .onSuccess(result -> resultHandler.handle(Future.succeededFuture("success")))
                .onFailure(cause -> resultHandler.handle(Future.failedFuture(cause)));
    }

    @Override
    public void refreshAllMetrics() {
        if (refreshRunning) {
            piveauLog.error("Refresh already running!");
            return;
        }
        refreshRunning = true;
        piveauLog.info("Start refreshing cache");
        long start = System.currentTimeMillis();
        dqvProvider.listCatalogues()
                .compose(list -> {
                    piveauLog.info("Found {} catalogues, first refreshing global", list.size());
                    return collectIterative(DocumentScope.GLOBAL, Collections.singletonList("global").iterator()).map(list);
                })
                .compose(list -> {
                    piveauLog.info("Global refreshed ({}), start refreshing catalogues", System.currentTimeMillis() - start);
                    return collectIterative(DocumentScope.CATALOGUE, list.iterator());
                }).compose(v -> {
                    piveauLog.info("Catalogues refreshed ({}), start refreshing countries", System.currentTimeMillis() - start);
                    return collectIterative(DocumentScope.COUNTRY, SparqlHelper.getCountries().keySet().iterator());
                }).onFailure(cause -> {
                    piveauLog.error("Refreshing cache", cause);
                }).onComplete(v -> {
                    piveauLog.info("Cache refresh finished ({} ms)", System.currentTimeMillis() - start);
                    refreshRunning = false;
                });
    }

    @Override
    public void refreshSingleMetrics(DocumentScope scope, String id) {
        collectMetricsForID(scope, id)
                .onSuccess(v -> {
                })
                .onFailure(cause -> {
                });
    }

    @Override
    public void moveScore() {
        dbClient.find(DocumentScope.SCORE.name(), new JsonObject())
                .onSuccess(list ->
                        list.forEach(doc -> {
                            String id = doc.getString("_id", "");
                            JsonArray scores = doc.getJsonArray("scores");

                            List<JsonObject> convertedScores = scores.stream().map(obj ->
                                    new JsonObject()
                                            .put("value", ((JsonObject) obj).getDouble("score"))
                                            .put("date", ((JsonObject) obj).getString("date"))
                            ).toList();

                            JsonArray newScores = new JsonArray(convertedScores);

                            //the update instructions, we want to add the score to an existing array, so we need to use $push,
                            //we have multiple values, so we need to use $each inside of the $push command
                            JsonObject update = new JsonObject().put("$push", new JsonObject().put("score", new JsonObject().put("$each", newScores)));

                            //this if is only there to see, which document scope we have to use, everything else stays the same
                            DocumentScope scope;
                            if (id.equals("global")) {
                                scope = DocumentScope.GLOBAL;
                            } else if (getCountries().containsKey(id)) {
                                scope = DocumentScope.COUNTRY;
                            } else {
                                scope = DocumentScope.CATALOGUE;
                            }
                            updateDocumentInDB(scope, id, update)
                                    .onSuccess(result -> log.debug("Score moved to new doc: {}", result.getDocUpsertedId()))
                                    .onFailure(cause -> log.error("Error while moving score", cause));
                        })
                );
    }

    /**
     * Wrapper for mongodb update collection for a single document
     *
     * @param documentScope the collection that document is stored in
     * @param id            the id of the document
     * @param update        the update object with instructions on how to update that document
     */
    private Future<MongoClientUpdateResult> updateDocumentInDB(DocumentScope documentScope, String id, JsonObject update) {
        JsonObject search = new JsonObject()
                .put("_id", id);

        return dbClient.updateCollectionWithOptions(
                documentScope.name(),
                search,
                update,
                new UpdateOptions().setUpsert(true));
    }

    @Override
    public void clearMetrics() {
        piveauLog.info("Start clearing metrics");
        for (DocumentScope metric : DocumentScope.values()) {
            dbClient.dropCollection(metric.name(), ar -> {
                if (ar.succeeded()) {
                    piveauLog.info("All metrics dropped");
                } else {
                    piveauLog.error("Dropping metrics failed", ar.cause());
                }
            });
        }
    }

    @Override
    public void getDistributionReachabilityDetails(String catalogueId, int offset, int limit, String lang, Handler<AsyncResult<JsonObject>> resultHandler) {
        dqvProvider.getDistributionReachabilityDetails(catalogueId, offset, limit, lang)
                .onSuccess(statusCodes -> {
                    JsonObject document = new JsonObject()
                            .put("success", "true")
                            .put("result", statusCodes);
                    resultHandler.handle(Future.succeededFuture(document));
                })
                .onFailure(cause -> resultHandler.handle(Future.failedFuture(cause)));
    }

    private Future<Void> collectIterative(DocumentScope scope, Iterator<String> iterator) {
        if (iterator.hasNext()) {
            String next = iterator.next();
            return collectMetricsForID(scope, next)
                    .compose(v -> collectIterative(scope, iterator));
        } else {
            return Future.succeededFuture();
        }
    }

    private Future<Void> collectMetricsForID(DocumentScope scope, String documentId) {
        JsonObject update = new JsonObject();
        JsonObject pushObject = new JsonObject();
        update.put("$push", pushObject);

        return dqvProvider.getTotalCount(scope, documentId)
                .compose(totalCount ->
                        addFindability(pushObject, scope, documentId)
                                .map(totalCount)
                )
                .compose(totalCount ->
                        addAccessibility(pushObject, scope, documentId, totalCount)
                                .map(totalCount)
                ).compose(totalCount ->
                        addInteroperability(pushObject, scope, documentId, totalCount)
                                .map(totalCount)
                ).compose(totalCount ->
                        addReusability(pushObject, scope, documentId, totalCount)
                                .map(totalCount)
                ).compose(totalCount ->
                        addContextuality(pushObject, scope, documentId, totalCount)
                ).map(v -> {
                            // summarize dimension scores instead of querying (quick and dirty)
                            Double findabilityScore = pushObject
                                    .getJsonObject("findability.score")
                                    .getJsonObject("value")
                                    .getDouble("points");
                            Double accessibilityScore = pushObject
                                    .getJsonObject("accessibility.score")
                                    .getJsonObject("value")
                                    .getDouble("points");
                            Double interoperabilityScore = pushObject
                                    .getJsonObject("interoperability.score")
                                    .getJsonObject("value")
                                    .getDouble("points");
                            Double reusabilityScore = pushObject
                                    .getJsonObject("reusability.score")
                                    .getJsonObject("value")
                                    .getDouble("points");
                            Double contextualityScore = pushObject
                                    .getJsonObject("contextuality.score")
                                    .getJsonObject("value")
                                    .getDouble("points");

                            return findabilityScore
                                    + accessibilityScore
                                    + interoperabilityScore
                                    + reusabilityScore
                                    + contextualityScore;

//                            return dqvProvider.getAverageScore(documentId, scope, PV.scoring.getURI());
                        }
                ).compose(result -> {
                    pushObject.put("score", new JsonObject()
                            .put("date", dateFormat.format(new Date())).put("value", result).put("max", getTotalMaxScore())
                    );
                    // saveScoreToTimeline(documentId, result);

                    if (scope == DocumentScope.CATALOGUE) {
                        return dqvProvider.getCatalogueInfo(documentId)
                                .onSuccess(info -> update.put("$set", new JsonObject().put("info", info)))
                                .mapEmpty();
                    } else {
                        return Future.succeededFuture();
                    }
                })
                .compose(v -> {
                    if (log.isDebugEnabled()) {
                        log.debug("Updated metrics for {}:\n{}", documentId, update.encodePrettily());
                    }
                    return updateDocumentInDB(scope, documentId, update);
                })
                .onSuccess(result -> log.info("Saved metrics for document {}", documentId))
                .onFailure(cause -> log.error("Failed to save metrics for document ID {}", documentId, cause))
                .mapEmpty();
    }

    private Future<Void> addFindability(JsonObject document, DocumentScope documentScope, String catalogueId) {
        return dqvProvider.getAverageScore(catalogueId, documentScope, PV.findabilityScoring.getURI())
                .compose(score -> {
                    document.put("findability.score", new JsonObject()
                            .put("date", dateFormat.format(new Date()))
                            .put("value", new JsonObject()
                                    .put("points", score)
                                    .put("percentage", getScorePercentage(score, Dimension.FINDABILITY))
                                    .put("max", getMaxScore(Dimension.FINDABILITY))
                            )
                    );
                    return dqvProvider.getKeywordAvailability(catalogueId, documentScope);
                }).compose(availability -> {
                    if (availability != -1.0) {
                        document.put("findability.keywordAvailability", getPercentageWithDate(availability));
                    }
                    return dqvProvider.getCategoryAvailability(catalogueId, documentScope);
                }).compose(availability -> {
                    if (availability != -1.0) {
                        document.put("findability.categoryAvailability", getPercentageWithDate(availability));
                    }
                    return dqvProvider.getSpatialAvailability(catalogueId, documentScope);
                }).compose(availability -> {
                    if (availability != -1.0) {
                        document.put("findability.spatialAvailability", getPercentageWithDate(availability));
                    }
                    return dqvProvider.getTemporalAvailability(catalogueId, documentScope);
                }).onSuccess(availability -> {
                    if (availability != -1.0) {
                        document.put("findability.temporalAvailability", getPercentageWithDate(availability));
                    }
                }).mapEmpty();
    }

    private Future<Void> addAccessibility(JsonObject document, DocumentScope documentScope, String catalogueId, Long totalCount) {
        return dqvProvider.getAverageScore(catalogueId, documentScope, PV.accessibilityScoring.getURI())
                .compose(score -> {
                    document.put("accessibility.score", new JsonObject()
                            .put("date", dateFormat.format(new Date()))
                            .put("value", new JsonObject()
                                    .put("points", score)
                                    .put("percentage", getScorePercentage(score, Dimension.ACCESSIBILITY))
                                    .put("max", getMaxScore(Dimension.ACCESSIBILITY))));
                    return dqvProvider.getAccessUrlStatusCodes(catalogueId, documentScope, totalCount);
                }).compose(statusCodes -> {
                    document.put("accessibility.accessUrlStatusCodes", wrapWithDate(statusCodes.toJsonArray()));
                    return dqvProvider.getAccessUrlAccessibility(catalogueId, documentScope, totalCount);
                }).compose(accessibility -> {
                    if (accessibility != -1.0) {
                        document.put("accessibility.accessUrlAccessibility", getPercentageWithDate(accessibility));
                    }
                    return dqvProvider.getDownloadUrlStatusCodes(catalogueId, documentScope, totalCount);
                }).compose(statusCodes -> {
                    document.put("accessibility.downloadUrlStatusCodes", wrapWithDate(statusCodes.toJsonArray()));
                    return dqvProvider.getDownloadUrlAccessibility(catalogueId, documentScope, totalCount);
                }).compose(accessibility -> {
                    if (accessibility != -1.0) {
                        document.put("accessibility.downloadUrlAccessibility", getPercentageWithDate(accessibility));
                    }
                    return dqvProvider.getDownloadUrlAvailability(catalogueId, documentScope, totalCount);
                }).onSuccess(availability -> {
                    if (availability != -1.0) {
                        document.put("accessibility.downloadUrlAvailability", getPercentageWithDate(availability));
                    }
                }).mapEmpty();
    }

    private Future<Void> addInteroperability(JsonObject document, DocumentScope documentScope, String catalogueId, Long totalCount) {
        return dqvProvider.getAverageScore(catalogueId, documentScope, PV.interoperabilityScoring.getURI())
                .compose(score -> {
                    document.put("interoperability.score", new JsonObject()
                            .put("date", dateFormat.format(new Date()))
                            .put("value", new JsonObject()
                                    .put("points", score)
                                    .put("percentage", getScorePercentage(score, Dimension.INTEROPERABILITY))
                                    .put("max", getMaxScore(Dimension.INTEROPERABILITY))));
                    return dqvProvider.getFormatAvailability(catalogueId, documentScope, totalCount);
                }).compose(result -> {
                    if (result != -1.0) {
                        document.put("interoperability.formatAvailability", getPercentageWithDate(result));
                    }
                    return dqvProvider.getMediaTypeAvailability(catalogueId, documentScope, totalCount);
                }).compose(result -> {
                    if (result != -1.0) {
                        document.put("interoperability.mediaTypeAvailability", getPercentageWithDate(result));
                    }
                    return dqvProvider.getFormatMediaTypeAlignment(catalogueId, documentScope, totalCount);
                }).compose(result -> {
                    if (result != -1.0) {
                        document.put("interoperability.formatMediaTypeAlignment", getPercentageWithDate(result));
                    }
                    return dqvProvider.getFormatMediaTypeNonProprietary(catalogueId, documentScope, totalCount);
                }).compose(result -> {
                    if (result != -1.0) {
                        document.put("interoperability.formatMediaTypeNonProprietary", getPercentageWithDate(result));
                    }
                    return dqvProvider.getFormatMediaTypeMachineReadable(catalogueId, documentScope, totalCount);
                }).compose(result -> {
                    if (result != -1.0) {
                        document.put("interoperability.formatMediaTypeMachineReadable", getPercentageWithDate(result));
                    }
                    return dqvProvider.getDcatApCompliance(catalogueId, documentScope);
                }).onSuccess(result -> {
                    if (result != -1.0) {
                        document.put("interoperability.dcatApCompliance", getPercentageWithDate(result));
                    }
                }).mapEmpty();
    }

    private Future<Void> addReusability(JsonObject document, DocumentScope documentScope, String catalogueId, Long totalCount) {
        return dqvProvider.getAverageScore(catalogueId, documentScope, PV.reusabilityScoring.getURI())
                .compose(score -> {
                    document.put("reusability.score", new JsonObject()
                            .put("date", dateFormat.format(new Date()))
                            .put("value", new JsonObject()
                                    .put("points", score)
                                    .put("percentage", getScorePercentage(score, Dimension.REUSABILIY))
                                    .put("max", getMaxScore(Dimension.REUSABILIY))));
                    return dqvProvider.getLicenceAvailability(catalogueId, documentScope, totalCount);
                }).compose(result -> {
                    if (result != -1.0) {
                        document.put("reusability.licenceAvailability", getPercentageWithDate(result));
                    }
                    return dqvProvider.getLicenceAlignment(catalogueId, documentScope, totalCount);
                }).compose(result -> {
                    if (result != -1.0) {
                        document.put("reusability.licenceAlignment", getPercentageWithDate(result));
                    }
                    return dqvProvider.getAccessRightsAvailability(catalogueId, documentScope);
                }).compose(result -> {
                    if (result != -1.0) {
                        document.put("reusability.accessRightsAvailability", getPercentageWithDate(result));
                    }
                    return dqvProvider.getAccessRightsAlignment(catalogueId, documentScope);
                }).compose(result -> {
                    if (result != -1.0) {
                        document.put("reusability.accessRightsAlignment", getPercentageWithDate(result));
                    }
                    return dqvProvider.getContactPointAvailability(catalogueId, documentScope);
                }).compose(result -> {
                    if (result != -1.0) {
                        document.put("reusability.contactPointAvailability", getPercentageWithDate(result));
                    }
                    return dqvProvider.getPublisherAvailability(catalogueId, documentScope);
                }).onSuccess(result -> {
                    if (result != -1.0) {
                        document.put("reusability.publisherAvailability", getPercentageWithDate(result));
                    }
                }).mapEmpty();
    }

    private Future<Void> addContextuality(JsonObject document, DocumentScope documentScope, String catalogueId, Long totalCount) {
        return dqvProvider.getAverageScore(catalogueId, documentScope, PV.contextualityScoring.getURI())
                .compose(score -> {
                    document.put("contextuality.score", new JsonObject()
                            .put("date", dateFormat.format(new Date()))
                            .put("value", new JsonObject()
                                    .put("points", score)
                                    .put("percentage", getScorePercentage(score, Dimension.CONTEXTUALITY))
                                    .put("max", getMaxScore(Dimension.CONTEXTUALITY))));
                    return dqvProvider.getRightsAvailability(catalogueId, documentScope, totalCount);
                }).compose(result -> {
                    if (result != -1.0) {
                        document.put("contextuality.rightsAvailability", getPercentageWithDate(result));
                    }
                    return dqvProvider.getByteSizeAvailability(catalogueId, documentScope, totalCount);
                }).compose(result -> {
                    if (result != -1.0) {
                        document.put("contextuality.byteSizeAvailability", getPercentageWithDate(result));
                    }
                    return dqvProvider.getDateIssuedAvailability(catalogueId, documentScope, totalCount);
                }).compose(result -> {
                    if (result != -1.0) {
                        document.put("contextuality.dateIssuedAvailability", getPercentageWithDate(result));
                    }
                    return dqvProvider.getDateModifiedAvailability(catalogueId, documentScope, totalCount);
                }).onSuccess(result -> {
                    if (result != -1.0) {
                        document.put("contextuality.dateModifiedAvailability", getPercentageWithDate(result));
                    }
                }).mapEmpty();
    }

    private JsonObject getPercentageWithDate(Double yesPercentage) {
        return wrapWithDate(PercentageMath.getYesNoPercentage(yesPercentage));
    }

    private JsonObject wrapWithDate(JsonArray jsonArray) {
        return new JsonObject()
                .put("date", dateFormat.format(new Date()))
                .put("value", jsonArray);
    }

    private Double getScorePercentage(Double score, Dimension dimension) {
        return (double) Math.round(score / dimension.getMaxScore() * 100);
    }

    private Double getMaxScore(Dimension dimension) {
        return (double) dimension.getMaxScore();
    }

    private Double getTotalMaxScore() {
        return getMaxScore(Dimension.FINDABILITY) +
                getMaxScore(Dimension.ACCESSIBILITY) +
                getMaxScore(Dimension.INTEROPERABILITY) +
                getMaxScore(Dimension.REUSABILIY) +
                getMaxScore(Dimension.CONTEXTUALITY);
    }
}
