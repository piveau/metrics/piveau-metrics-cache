package io.piveau.metrics.cache;

import io.piveau.dcatap.DCATAPUriSchema;
import io.piveau.json.ConfigHelper;
import io.piveau.metrics.cache.dqv.DqvProvider;
import io.piveau.metrics.cache.dqv.DqvVerticle;
import io.piveau.metrics.cache.dqv.sparql.QueryCollection;
import io.piveau.metrics.cache.dqv.sparql.util.SparqlHelper;
import io.piveau.metrics.cache.persistence.DatabaseProvider;
import io.piveau.metrics.cache.persistence.DatabaseVerticle;
import io.piveau.metrics.cache.persistence.DocumentScope;
import io.piveau.metrics.cache.quartz.QuartzService;
import io.piveau.metrics.cache.quartz.QuartzServiceVerticle;
import io.piveau.security.ApiKeyAuthProvider;
import io.piveau.utils.ConfigurableAssetHandler;
import io.vertx.config.ConfigRetriever;
import io.vertx.core.*;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.healthchecks.HealthCheckHandler;
import io.vertx.ext.healthchecks.Status;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.handler.APIKeyHandler;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.ext.web.openapi.RouterBuilder;
import io.vertx.ext.web.openapi.RouterBuilderOptions;
import io.vertx.ext.web.validation.BadRequestException;
import io.vertx.ext.web.validation.RequestParameters;
import io.vertx.ext.web.validation.ValidationHandler;
import io.vertx.serviceproxy.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


public class MainVerticle extends AbstractVerticle {

    private static final Logger log = LoggerFactory.getLogger(MainVerticle.class);

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    private JsonObject buildInfo;
    private DatabaseProvider databaseProvider;

    private QuartzService quartzService;
    private DqvProvider dqvProvider;

    public static void main(String[] args) {
        String[] params = Arrays.copyOf(args, args.length + 1);
        params[params.length - 1] = MainVerticle.class.getName();
        Launcher.executeCommand("run", params);
    }

    @Override
    public void start(Promise<Void> startPromise) {
        log.info("Launching cache service...");

        QueryCollection.init(vertx, "queries");

        // startup is only successful if no step failed
        loadConfig()
                .compose(this::bootstrapVerticles)
                .compose(this::startServer)
                .onComplete(startPromise);
    }

    private Future<JsonObject> loadConfig() {
        return ConfigRetriever.create(vertx)
                .getConfig()
                .onSuccess(config -> {
                    if (log.isDebugEnabled()) {
                        log.debug(config.encodePrettily());
                    }
                    JsonObject schemaConfig = ConfigHelper.forConfig(config).forceJsonObject(ApplicationConfig.ENV_PIVEAU_DCATAP_SCHEMA_CONFIG);
                    if (schemaConfig.isEmpty()) {
                        schemaConfig.put("baseUri", config.getString(ApplicationConfig.ENV_BASE_URI, DCATAPUriSchema.DEFAULT_BASE_URI));
                    }
                    DCATAPUriSchema.setConfig(schemaConfig);
                });
    }

    private Future<JsonObject> bootstrapVerticles(JsonObject config) {
        JsonObject tripleStoreConfig = ConfigHelper.forConfig(config).forceJsonObject(ApplicationConfig.ENV_PIVEAU_TRIPLESTORE_CONFIG);
        return vertx.deployVerticle(DqvVerticle.class, new DeploymentOptions()
                        .setConfig(tripleStoreConfig)
                        .setThreadingModel(ThreadingModel.WORKER))
                .compose(id -> vertx.deployVerticle(DatabaseVerticle.class, new DeploymentOptions()
                        .setConfig(config)
                        .setThreadingModel(ThreadingModel.WORKER)))
                .compose(id -> {
                    if (Boolean.TRUE.equals(config.getBoolean(ApplicationConfig.ENV_PIVEAU_CACHE_SCHEDULER_ENABLED, false))) {
                        return vertx.deployVerticle(QuartzServiceVerticle.class, new DeploymentOptions()
                                .setThreadingModel(ThreadingModel.WORKER)
                                .setConfig(config));
                    } else {
                        return Future.succeededFuture(id);
                    }
                })
                .map(config);
    }

    private Future<Void> startServer(JsonObject config) {


        Buffer buffer = vertx.fileSystem().readFileBlocking("buildInfo.json");
        if (buffer != null) {
            buildInfo = buffer.toJsonObject();
        } else {
            buildInfo = new JsonObject()
                    .put("timestamp", "Build time not available")
                    .put("version", "Version not available");
        }


        databaseProvider = DatabaseProvider.createProxy(vertx, DatabaseProvider.SERVICE_ADDRESS);
        Integer port = config.getInteger(ApplicationConfig.ENV_APPLICATION_PORT, ApplicationConfig.DEFAULT_APPLICATION_PORT);
//FIXME
        DeliveryOptions options1 = new DeliveryOptions().setSendTimeout(3000000);
        dqvProvider = DqvProvider.createProxy(vertx, DqvProvider.SERVICE_ADDRESS, options1);

        quartzService = QuartzService.createProxy(vertx, QuartzService.SERVICE_ADDRESS);

        return RouterBuilder.create(vertx, "webroot/openapi.yaml")
                .compose(builder -> {

                    RouterBuilderOptions options = new RouterBuilderOptions().setMountNotImplementedHandler(true);
                    builder.setOptions(options);

                    JsonArray corsDomains = ConfigHelper.forConfig(config).forceJsonArray(ApplicationConfig.ENV_CACHE_CORS_DOMAINS);
                    if (!corsDomains.isEmpty()) {

                        Set<String> allowedHeaders = Set.of(
                                "x-requested-with",
                                "Access-Control-Allow-Origin",
                                "Origin",
                                "Content-Type",
                                "Accept",
                                "Authorization",
                                "X-API-Key"
                        );

                        Set<HttpMethod> allowedMethods = Set.of(
                                HttpMethod.GET,
                                HttpMethod.POST,
                                HttpMethod.OPTIONS,
                                HttpMethod.DELETE,
                                HttpMethod.PATCH,
                                HttpMethod.PUT
                        );

                        ArrayList<String> corsArray = new ArrayList<>();
                        for (int i = 0; i < corsDomains.size(); i++) {
                            //convert into normal array and escape dots for regex compatibility
                            corsArray.add(corsDomains.getString(i).replace(".", "\\."));
                        }

                        //"^(https?:\\/\\/(?:.+\\.)?(?:fokus\\.fraunhofer\\.de|localhost)(?::\\d{1,5})?)$"
                        String corsString = "^(https?:\\/\\/(?:.+\\.)?(?:" + String.join("|", corsArray) + ")(?::\\d{1,5})?)$";
                        builder.rootHandler(CorsHandler.create(corsString).allowedHeaders(allowedHeaders).allowedMethods(allowedMethods).allowCredentials(true));
                    } else {
                        log.warn("No CORS domains configured");
                    }
                    builder.rootHandler(BodyHandler.create());
                    builder.rootHandler(StaticHandler.create());

                    try {
                        String apiKey = config.getString(ApplicationConfig.ENV_APIKEY, "apiKey");

                        if (apiKey == null || apiKey.isEmpty()) {
                            throw new Exception("API key is not configured. Please provide a valid API key.");
                        }

                        builder.securityHandler("ApiKeyAuth", APIKeyHandler.create(new ApiKeyAuthProvider(apiKey)).header("Authorization"));

                    } catch (Exception e) {
                        log.error(e.getMessage());
                    }

                    // Administration
                    builder.operation("refreshAllMetrics").handler(this::refreshAllMetrics);
                    builder.operation("refreshSingleMetrics").handler(this::refreshSingleMetrics);
                    builder.operation("clearMetrics").handler(this::clearMetrics);
                    builder.operation("migrateScore").handler(this::migrateScore);
                    builder.operation("scheduleRefresh").handler(this::scheduleRefresh);
                    builder.operation("getScheduleRefresh").handler(this::getSchedules);

                    // Global
                    builder.operation("getGlobalMetrics").handler(context -> fetchCurrent(DocumentScope.GLOBAL, context));
                    builder.operation("getHistoricGlobalMetrics").handler(context -> fetchHistory(DocumentScope.GLOBAL, context));

                    // Countries
                    builder.operation("getSingleCountryMetrics").handler(context -> fetchCurrent(DocumentScope.COUNTRY, context));
                    builder.operation("getHistoricSingleCountryMetrics").handler(context -> fetchHistory(DocumentScope.COUNTRY, context));
                    builder.operation("getCountryMetrics").handler(context -> fetchCurrentList(DocumentScope.COUNTRY, context));
                    builder.operation("getHistoricCountryMetrics").handler(context -> fetchHistoryList(DocumentScope.COUNTRY, context));

                    // Catalogues
                    builder.operation("getSingleCatalogueMetrics").handler(context -> fetchCurrent(DocumentScope.CATALOGUE, context));
                    builder.operation("getHistoricSingleCatalogueMetrics").handler(context -> fetchHistory(DocumentScope.CATALOGUE, context));
                    builder.operation("getCatalogueMetrics").handler(context -> fetchCurrentList(DocumentScope.CATALOGUE, context));
                    builder.operation("getHistoricCatalogueMetrics").handler(context -> fetchHistoryList(DocumentScope.CATALOGUE, context));
                    builder.operation("deleteCatalogueMetrics").handler(this::deleteCatalogueMetrics);
                    builder.operation("getCatalogueDistributionReachability").handler(this::getCatalogueDistributionReachability);
                    builder.operation("getCatalogueViolations").handler(this::getViolations);

                    // Datasets
                    builder.operation("getSingleDatasetMetrics").handler(this::getSingleDatasetMetrics);
                    builder.operation("getDistributionMetrics").handler(this::getAllDistributionMetrics);
                    builder.operation("getDistributionMetricsv2").handler(this::getAllDistributionMetrics);

                    WebClient client = WebClient.create(vertx);

                    Router router = builder.createRouter();
                    router.route("/images/favicon").handler(new ConfigurableAssetHandler(config.getString("PIVEAU_FAVICON_PATH", "webroot/images/favicon.png"), client));
                    router.route("/images/logo").handler(new ConfigurableAssetHandler(config.getString("PIVEAU_LOGO_PATH", "webroot/images/logo.png"), client));

                    router.route("/imprint").handler(context ->
                            context.redirect(config.getString("PIVEAU_IMPRINT_URL", "/")));

                    router.route("/privacy").handler(context ->
                            context.redirect(config.getString("PIVEAU_PRIVACY_URL", "/")));

                    HealthCheckHandler hch = HealthCheckHandler.create(vertx);
                    hch.register("buildInfo", status -> status.complete(Status.OK(buildInfo)));
                    router.get("/health").handler(hch);
                    log.info("health added");

                    router.errorHandler(400, context -> {
                        if (context.failure() instanceof BadRequestException bre) {
                            JsonObject error = new JsonObject()
                                    .put("status", "error")
                                    .put("message", bre.getMessage());
                            context.response().setStatusCode(400).putHeader("Content-Type", "application/json").end(error.encodePrettily());
                        }
                    });

                    return vertx.createHttpServer(new HttpServerOptions().setPort(port)).requestHandler(router).listen()
                            .onSuccess(server -> {
                                log.info("Cache service started on port {}", port);
                            })
                            .onFailure(cause -> {
                                log.error("Failed to create cache server on port {}", port, cause);
                            });
                }).mapEmpty();
    }

    private void fetchHistoryList(DocumentScope documentScope, RoutingContext context) {

        MultiMap queryParams = context.queryParams();

        String resolution = queryParams.contains("resolution") ? queryParams.get("resolution") : "month";

        String startDate = queryParams.get("startDate");
        String endDate = queryParams.contains("endDate") ? queryParams.get("endDate") : dateFormat.format(new Date());

        if (!validDates(startDate, endDate, context)) {
            context.fail(400);
            return;
        }

        List<String> filters = getFilterParam(queryParams);

        databaseProvider.getHistoryList(documentScope, filters, resolution, startDate, endDate, fetchHandler(context));
    }

    /**
     * This method validates the formatting of the dates and that the timing of the start and end dates are valid.
     * When returning from a non valid input, it also sets the server response
     *
     * @param startDate
     * @param endDate
     * @param context
     * @return
     */
    private boolean validDates(String startDate, String endDate, RoutingContext context) {
        try {
            if (dateFormat.parse(startDate).after(dateFormat.parse(endDate))) {
                context.response().setStatusCode(400).setStatusMessage("Bad Request - startDate after endDate").end();
                return false;
            }
        } catch (ParseException e) {
            context.response().setStatusCode(400).end(e.getMessage());
            return false;
        }
        return true;
    }

    private void fetchCurrentList(DocumentScope documentScope, RoutingContext context) {

        List<String> filters = getFilterParam(context.queryParams());
        databaseProvider.getDocumentList(documentScope, filters, fetchHandler(context));

    }

    private void fetchHistory(DocumentScope documentScope, RoutingContext context) {

        String id = "global";
        if (documentScope != DocumentScope.GLOBAL) {
            id = context.pathParam("id");
            if (id == null) {
                context.response().setStatusCode(500).end("Cannot read document ID");
                return;
            }
        }

        MultiMap queryParams = context.queryParams();


        String resolution = queryParams.contains("resolution") ? queryParams.get("resolution") : "month";

        String startDate = queryParams.get("startDate");
        String endDate = queryParams.contains("endDate") ? queryParams.get("endDate") : dateFormat.format(new Date());

        if (!validDates(startDate, endDate, context)) {
            return;
        }

        List<String> filters = getFilterParam(queryParams);

        databaseProvider.getHistory(documentScope, id, filters, resolution, startDate, endDate, fetchHandler(context));

    }


    private void fetchCurrent(DocumentScope documentScope, RoutingContext context) {


        String id = "global";
        if (documentScope != DocumentScope.GLOBAL) {
            id = context.pathParam("id");
            if (id == null) {
                context.response().setStatusCode(500).end("Cannot read document ID");
            }
        }


        List<String> filters = getFilterParam(context.queryParams());

        if (log.isDebugEnabled()) {
            log.debug("Filter:");
            filters.forEach(log::debug);
        }

        databaseProvider.getDocument(documentScope, id, filters, ar -> {
            if (ar.succeeded()) {
                if (ar.result() == null) {
                    context.response().setStatusCode(404).end();
                } else {
                    context.response()
                            .setStatusCode(200)
                            .putHeader("Content-Type", "application/json")
                            .end(ar.result().encodePrettily());
                }
            } else {
                context.response().setStatusCode(500).end(ar.cause().getMessage());
            }
        });

    }

    private List<String> getFilterParam(MultiMap queryParams) {
        List<String> filters = new ArrayList<>();

        if (queryParams.contains("filter")) {
            filters = queryParams.getAll("filter");
        }

        //This param should work with comma separated values but something is not working here?!
        //see https://swagger.io/docs/specification/serialization/#query
        //
        //style     explode     Primitive value id = 5      Array id = [3, 4, 5]
        //form *    true *      /users?id=5                 /users?id=3&id=4&id=5
        //form 	    false       /users?id=5 	            /users?id=3,4,5  <- THIS!

        //So we have to split it ourselves
        if (filters.size() == 1) filters = Arrays.asList(filters.get(0).split(","));

        return filters;
    }

    private Promise<JsonObject> fetchHandler(RoutingContext context) {
        Promise<JsonObject> p = Promise.promise();

        p.future().onFailure(err -> context.response().setStatusCode(500).end(err.getMessage()));

        p.future().onSuccess(result -> {
            if (result == null) {
                context.response().setStatusCode(404).end();
            } else {
                context.response()
                        .setStatusCode(200)
                        .putHeader("Content-Type", "application/json")
                        .end(result.encodePrettily());
            }
        });
        return p;
    }


    private void deleteCatalogueMetrics(RoutingContext context) {
        databaseProvider.deleteDocument(DocumentScope.CATALOGUE, context.pathParam("id"), ar -> {
            if (ar.succeeded()) {
                String result = ar.result();
                if (result.equals("success")) {
                    context.response().setStatusCode(200).end();
                } else if (result.equals("not found")) {
                    context.response().setStatusCode(404).end();
                } else {
                    context.response().setStatusCode(400).setStatusMessage(result).end();
                }
            } else {
                context.response().setStatusCode(500).end(ar.cause().getMessage());
            }
        });
    }

    private void migrateScore(RoutingContext context) {
        databaseProvider.moveScore();
        context.response().setStatusCode(202).end();
    }

    private void refreshAllMetrics(RoutingContext context) {
        databaseProvider.refreshAllMetrics();
        context.response().setStatusCode(202).end();
    }

    private void refreshSingleMetrics(RoutingContext context) {
        String id = context.pathParam("id");
        String scope = context.pathParam("scope");

        if ("catalogues".equals(scope)) {
            dqvProvider.listCatalogues()
                    .onSuccess(catalogues -> {
                        if (catalogues.contains(id)) {
                            databaseProvider.refreshSingleMetrics(DocumentScope.CATALOGUE, id);
                            context.response().setStatusCode(202).end();
                        } else {
                            context.fail(404);
                        }
                    })
                    .onFailure(cause -> {
                        log.error("Fetching catalogue infos", cause);
                        context.fail(500);
                    });
        } else if ("countries".equals(scope)) {
            if (SparqlHelper.getCountries().containsKey(scope.toUpperCase())) {
                databaseProvider.refreshSingleMetrics(DocumentScope.COUNTRY, id);
                context.response().setStatusCode(202).end();
            } else {
                context.fail(400);
            }
        } else {
            databaseProvider.refreshSingleMetrics(DocumentScope.GLOBAL, "global");
            context.response().setStatusCode(202).end();
        }
    }

    private void clearMetrics(RoutingContext context) {
        databaseProvider.clearMetrics();
        context.response().setStatusCode(202).end();
    }

    /**
     * getViolations method is operation connected to endpoint  /metrics/catalogues/<catalogue>/violation
     *
     * @param context the RoutingContext
     */
    private void getViolations(RoutingContext context) {
        String id = getID(context);

        MultiMap params = context.queryParams();
        String lang = "en";
        if (params.contains("locale")) {
            lang = params.get("locale");
        } else if (!context.acceptableLanguages().isEmpty()) {
            lang = context.acceptableLanguages().get(0).tag();
        }

        int offset = params.contains("offset") ? Integer.parseInt(params.get("offset")) : 0;
        int limit = params.contains("limit") ? Integer.parseInt(params.get("limit")) : 100;

        dqvProvider.getCatalogueViolations(id, offset, limit, lang)
                .onSuccess(result -> context.response()
                        .setStatusCode(200)
                        .putHeader("Content-Type", "application/json")
                        .end(result.encodePrettily())
                )
                .onFailure(cause -> {
                    log.error("Failed to get violations", cause);
                    context.response().setStatusCode(500).end();
                });
    }

    /**
     * get error status codes method get error status codes for all distributions of a dataset for a specific catalogue.
     * and put it JSON structure for the response:
     * {
     * "success": true,
     * "result": {
     * "count": 47,
     * "results": [
     * {
     *
     * @param context the routing context
     */
    private void getCatalogueDistributionReachability(RoutingContext context) {
        String id = getID(context);

        MultiMap params = context.queryParams();
        String lang = "en";
        if (params.contains("locale")) {
            lang = params.get("locale");
        } else if (!context.acceptableLanguages().isEmpty()) {
            lang = context.acceptableLanguages().get(0).tag();
        }

        int offset = params.contains("offset") ? Integer.parseInt(params.get("offset")) : 0;
        int limit = params.contains("limit") ? Integer.parseInt(params.get("limit")) : 100;

        databaseProvider.getDistributionReachabilityDetails(id, offset, limit, lang, ar -> {
            if (ar.succeeded()) {
                context.response()
                        .setStatusCode(200)
                        .putHeader("Content-Type", "application/json")
                        .end(ar.result().encodePrettily());
            } else {
                log.error("Failed to get reachability issues", ar.cause());
                // differentiate: e.g. id not found -> 404
                context.response().setStatusCode(500).end();
            }
        });
    }

    private String getID(RoutingContext context) {
        return context.pathParam("id");
    }

    private void getSingleDatasetMetrics(RoutingContext context) {
        MultiMap params = context.queryParams();
        String lang = "en";
        if (params.contains("locale")) {
            lang = params.get("locale");
        } else if (!context.acceptableLanguages().isEmpty()) {
            lang = context.acceptableLanguages().get(0).tag();
        }

        String datasetId = getID(context);
        dqvProvider.getDatasetMetrics(datasetId, lang)
                .onSuccess(result -> context.response()
                        .setStatusCode(200)
                        .putHeader("Content-Type", "application/json")
                        .end(result.encodePrettily())
                )
                .onFailure(cause -> context.response().setStatusCode(404).end());
    }

    private void getAllDistributionMetrics(RoutingContext context) {
        MultiMap params = context.queryParams();
        String lang = "en";
        if (params.contains("locale")) {
            lang = params.get("locale");
        } else if (!context.acceptableLanguages().isEmpty()) {
            lang = context.acceptableLanguages().get(0).tag();
        }

        String datasetId = getID(context);
        dqvProvider.getDistributionMetricsPerDataset(datasetId, lang)
                .onSuccess(result -> context.response()
                        .setStatusCode(200)
                        .putHeader("Content-Type", "application/json")
                        .end(result.encodePrettily())
                )
                .onFailure(cause -> context.response().setStatusCode(404).end());
    }

    private void scheduleRefresh(RoutingContext context) {

        RequestParameters params = context.get(ValidationHandler.REQUEST_CONTEXT_KEY);

        JsonObject trigger = params.body().getJsonObject();
        String id = UUID.randomUUID().toString();

        quartzService.putTrigger(QuartzService.ALL_CATALOGUES, id, trigger)
                .onSuccess(a -> {
                    int code = "created".equalsIgnoreCase(a) ? 201 : 200;
                    context.response().setStatusCode(code).end();
                })
                .onFailure(err -> {
                    err.printStackTrace();
                    context.response().setStatusCode(((ServiceException) err).failureCode()).end(err.getMessage());
                });


    }

    private void getSchedules(RoutingContext context) {
        quartzService.listTriggers().onFailure(err -> context.response().setStatusCode(((ServiceException) err).failureCode()).end(err.getMessage()))
                .onSuccess(t -> {
                    context.response()
                            .setStatusCode(200)
                            .putHeader("Content-Type", "application/json")
                            .end(t.encodePrettily());
                });
    }

}
