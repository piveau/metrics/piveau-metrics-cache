package io.piveau.metrics.cache.dqv;

import io.piveau.dcatap.DCATAPUriRef;
import io.piveau.dcatap.DCATAPUriSchema;
import io.piveau.dcatap.TripleStore;
import io.piveau.metrics.cache.dqv.sparql.QueryCollection;
import io.piveau.metrics.cache.dqv.sparql.handler.*;
import io.piveau.metrics.cache.dqv.sparql.util.PercentageMath;
import io.piveau.metrics.cache.persistence.DocumentScope;
import io.piveau.vocabularies.vocabulary.PV;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.serviceproxy.ServiceException;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.rdf.model.Resource;
import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

import static io.piveau.utils.DateTimeKt.normalizeDateTime;

class DqvProviderImpl implements DqvProvider {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final Cache<String, Future> violationsCache;
    private final Cache<String, Future> violationsCountCache;
    private final Cache<String, LocalDateTime> lastModifiedCache;
    private final TripleStore tripleStore;

    private final Map<String, JsonObject> catalogueInfos = new HashMap<>();

    DqvProviderImpl(TripleStore tripleStore) {
        this.tripleStore = tripleStore;

        CacheConfigurationBuilder<String, Future> cacheConfigurationBuilder = CacheConfigurationBuilder
                .newCacheConfigurationBuilder(String.class, Future.class, ResourcePoolsBuilder.heap(1000)).withExpiry(ExpiryPolicyBuilder.noExpiration());

        CacheManager cacheManager = CacheManagerBuilder.newCacheManagerBuilder()
                .withCache("violations", cacheConfigurationBuilder)
                .build(true);

        violationsCache = cacheManager.getCache("violations", String.class, Future.class);

        CacheManager cacheManager2 = CacheManagerBuilder.newCacheManagerBuilder()
                .withCache("violationsCount", cacheConfigurationBuilder)
                .build(true);

        violationsCountCache = cacheManager2.getCache("violationsCount", String.class, Future.class);


        CacheManager cacheManager3 = CacheManagerBuilder.newCacheManagerBuilder()
                .withCache("lastModified",
                        CacheConfigurationBuilder.newCacheConfigurationBuilder(String.class, LocalDateTime.class,
                                        ResourcePoolsBuilder.heap(1000))
                                .withExpiry(ExpiryPolicyBuilder.noExpiration()))
                .build(true);

        lastModifiedCache = cacheManager3.getCache("lastModified", String.class, LocalDateTime.class);
    }

    @Override
    public Future<List<String>> listCatalogues() {
        return getCatalogueList();
    }

    @Override
    public Future<Long> getTotalCount(DocumentScope documentScope, String id) {
        return TotalCountQueryHandler.create(id, documentScope)
                .queryFrom(tripleStore)
                .map(result -> result.getLong("count"))
                .otherwise(cause -> -1L);
    }

    @Override
    public Future<Double> getKeywordAvailability(String id, DocumentScope documentScope) {
        return getYesPercentage(PV.keywordAvailability, documentScope, id, -1L);
    }

    @Override
    public Future<Double> getCategoryAvailability(String id, DocumentScope documentScope) {
        return getYesPercentage(PV.categoryAvailability, documentScope, id, -1L);
    }

    @Override
    public Future<Double> getSpatialAvailability(String id, DocumentScope documentScope) {
        return getYesPercentage(PV.spatialAvailability, documentScope, id, -1L);
    }

    @Override
    public Future<Double> getTemporalAvailability(String id, DocumentScope documentScope) {
        return getYesPercentage(PV.temporalAvailability, documentScope, id, -1L);
    }

    @Override
    public Future<Double> getAccessUrlAccessibility(String id, DocumentScope documentScope, Long totalCount) {
        return getYesPercentage(PV.accessUrlStatusCode, documentScope, id, totalCount);
    }

    @Override
    public Future<StatusCodes> getAccessUrlStatusCodes(String id, DocumentScope documentScope, Long totalCount) {
        return getStatusCodes(PV.accessUrlStatusCode, documentScope, id, totalCount);
    }

    @Override
    public Future<Double> getDownloadUrlAvailability(String id, DocumentScope documentScope, Long totalCount) {
        return getYesPercentage(PV.downloadUrlAvailability, documentScope, id, totalCount);
    }

    @Override
    public Future<Double> getDownloadUrlAccessibility(String id, DocumentScope documentScope, Long totalCount) {
        return getYesPercentage(PV.downloadUrlStatusCode, documentScope, id, totalCount);
    }

    @Override
    public Future<JsonObject> getDistributionReachabilityDetails(String catalogueId, int offset, int limit, String lang) {
        return getErrorStatusCodes(catalogueId, offset, limit, lang);
    }

    @Override
    public Future<StatusCodes> getDownloadUrlStatusCodes(String catalogueId, DocumentScope documentScope, Long totalCount) {
        return getStatusCodes(PV.downloadUrlStatusCode, documentScope, catalogueId, totalCount);
    }

    @Override
    public Future<Double> getFormatAvailability(String id, DocumentScope documentScope, Long totalCount) {
        return getYesPercentage(PV.formatAvailability, documentScope, id, totalCount);
    }

    @Override
    public Future<Double> getMediaTypeAvailability(String id, DocumentScope documentScope, Long totalCount) {
        return getYesPercentage(PV.mediaTypeAvailability, documentScope, id, totalCount);
    }

    @Override
    public Future<Double> getFormatMediaTypeAlignment(String id, DocumentScope documentScope, Long totalCount) {
        return getYesPercentage(PV.formatMediaTypeVocabularyAlignment, documentScope, id, totalCount);
    }

    @Override
    public Future<Double> getFormatMediaTypeNonProprietary(String id, DocumentScope documentScope, Long totalCount) {
        return getYesPercentage(PV.formatMediaTypeNonProprietary, documentScope, id, totalCount);
    }

    @Override
    public Future<Double> getFormatMediaTypeMachineReadable(String id, DocumentScope documentScope, Long totalCount) {
        return getYesPercentage(PV.formatMediaTypeMachineInterpretable, documentScope, id, totalCount);
    }

    @Override
    public Future<Double> getDcatApCompliance(String id, DocumentScope documentScope) {
        return getYesPercentage(PV.dcatApCompliance, documentScope, id, -1L);
    }

    @Override
    public Future<Double> getLicenceAvailability(String id, DocumentScope documentScope, Long totalCount) {
        return getYesPercentage(PV.licenceAvailability, documentScope, id, totalCount);
    }

    @Override
    public Future<Double> getLicenceAlignment(String id, DocumentScope documentScope, Long totalCount) {
        return getYesPercentage(PV.knownLicence, documentScope, id, totalCount);
    }

    @Override
    public Future<Double> getAccessRightsAvailability(String id, DocumentScope documentScope) {
        return getYesPercentage(PV.accessRightsAvailability, documentScope, id, -1L);
    }

    @Override
    public Future<Double> getAccessRightsAlignment(String id, DocumentScope documentScope) {
        return getYesPercentage(PV.accessRightsVocabularyAlignment, documentScope, id, -1L);
    }

    @Override
    public Future<Double> getContactPointAvailability(String id, DocumentScope documentScope) {
        return getYesPercentage(PV.contactPointAvailability, documentScope, id, -1L);
    }

    @Override
    public Future<Double> getPublisherAvailability(String id, DocumentScope documentScope) {
        return getYesPercentage(PV.publisherAvailability, documentScope, id, -1L);
    }

    @Override
    public Future<Double> getRightsAvailability(String id, DocumentScope documentScope, Long totalCount) {
        return getYesPercentage(PV.rightsAvailability, documentScope, id, totalCount);
    }

    @Override
    public Future<Double> getByteSizeAvailability(String id, DocumentScope documentScope, Long totalCount) {
        return getYesPercentage(PV.byteSizeAvailability, documentScope, id, totalCount);
    }

    @Override
    public Future<Double> getDateIssuedAvailability(String id, DocumentScope documentScope, Long totalCount) {
        return getYesPercentage(PV.dateIssuedAvailability, documentScope, id, totalCount);
    }

    @Override
    public Future<Double> getDateModifiedAvailability(String id, DocumentScope documentScope, Long totalCount) {
        return getYesPercentage(PV.dateModifiedAvailability, documentScope, id, totalCount);
    }

    @Override
    public Future<Double> getAverageScore(String id, DocumentScope documentScope, String measurementUriRef) {
        return ScoreQueryHandler.create(id, documentScope, measurementUriRef)
                .queryFrom(tripleStore)
                .map(result -> {
                    if (!result.isEmpty()) {
                        return Double.valueOf(Math.round(result.getDouble("averageScore")));
                    } else {
                        return 0.0;
                    }
                });
    }

    @Override
    public Future<JsonObject> getCatalogueInfo(String id) {
        return Future.succeededFuture(catalogueInfos.get(id));
    }

    /**
     * Set the result Handler for the getViolationsCount
     *
     * @param id Catalogue id
     */
    @Override
    public Future<JsonObject> getCatalogueViolationsCount(String id) {
        DCATAPUriRef uriRef = DCATAPUriSchema.createForCatalogue(id);
        return new ViolationsCountQueryHandler(uriRef.getCatalogueUriRef())
                .queryFrom(tripleStore)
                .map(result -> result.put("success", true))
                .otherwise(cause -> new JsonObject()
                        .put("success", false)
                        .put("count", -1));
    }

    /**
     * Set the result Handler for the getViolations
     *
     * @param id     Catalogue id
     * @param offset Number of violations you wish to skip before selecting violations.
     * @param limit  Number of results returned
     * @param lang   Preferred language for the title
     */
    @Override
    public Future<JsonObject> getCatalogueViolations(String id, int offset, int limit, String lang) {
        DCATAPUriRef uriRef = DCATAPUriSchema.createForCatalogue(id);

        final AtomicReference<Boolean> useCached = new AtomicReference<>(Boolean.TRUE);
        final AtomicReference<JsonObject> resultReference = new AtomicReference<>(new JsonObject());

        return new CatalogueModifiedQueryHandler(uriRef.getCatalogueUriRef())
                .queryFrom(tripleStore)
                .compose(catalogueModified -> {
                    String normalizedTime = normalizeDateTime(catalogueModified.getString("modified", ""));
                    if (normalizedTime != null) {
                        try {
                            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
                            LocalDateTime modified = LocalDateTime.parse(normalizedTime, formatter);
                            useCached.set(!lastModifiedCache.containsKey(id) || !lastModifiedCache.get(id).isBefore(modified));
                            if (!lastModifiedCache.containsKey(id)) {
                                lastModifiedCache.put(id, modified);
                            }
                        } catch (Exception e) {
                            useCached.set(Boolean.FALSE);
                        }
                    }

                    Future<JsonObject> countFuture;
                    if (violationsCountCache.containsKey(id) && useCached.get()) {
                        countFuture = violationsCountCache.get(id);
                    } else {
                        countFuture = new ViolationsCountQueryHandler(uriRef.getCatalogueUriRef()).queryFrom(tripleStore);
                        violationsCountCache.put(id, countFuture);
                    }
                    return countFuture;
                }).compose(result -> {
                    resultReference.get()
                            .put("success", true)
                            .put("result", result);
                    String cacheKey = id + "-" + offset + "-" + limit + "-" + lang;
                    Future<JsonObject> violationsFuture;
                    if (violationsCache.containsKey(cacheKey) && useCached.get()) {
                        violationsFuture = violationsCache.get(cacheKey);
                    } else {
                        violationsFuture = new ViolationsQueryHandler(uriRef.getCatalogueUriRef(), offset, limit)
                                .queryFrom(tripleStore);
                        violationsCache.put(cacheKey, violationsFuture);
                    }
                    return violationsFuture;
                }).compose(violations -> {
                    resultReference.get().getJsonObject("result").put("results", violations.getJsonArray("violations"));
                    return resolveDatasetTitle(violations.getJsonArray("violations").iterator(), lang, new HashMap<>())
                            .map(resultReference.get());
                });
    }

    /**
     * Get all Metrics for a dataset and its distributions
     *
     * @param datasetId Dataset id
     */
    @Override
    public Future<JsonObject> getDatasetMetrics(String datasetId, String lang) {
        DCATAPUriRef schema = DCATAPUriSchema.createFor(datasetId);
        String graphName = schema.getMetricsGraphName();

        return tripleStore.getGraph(graphName)
                .map(model -> {
                    if (!model.isEmpty()) {
                        return new DatasetMetrics(model, tripleStore, lang).getDatasetMetrics();
                    } else {
                        throw new ServiceException(404, "DQV of dataset not found");
                    }
                });
    }

    @Override
    public Future<JsonObject> getDistributionMetricsPerDataset(String datasetId, String lang) {
        DCATAPUriRef schema = DCATAPUriSchema.createForDataset(datasetId);
        String graphName = schema.getMetricsGraphName();

        return tripleStore.getGraph(graphName)
                .compose(model -> {
                    if (model.isEmpty()) {
                        return Future.failedFuture(new ServiceException(404, "Dataset not found"));
                    } else {
                        DatasetMetrics datasetMetrics = new DatasetMetrics(model, tripleStore, lang);
                        return datasetMetrics.getDistributionMetricsPerDataset();
                    }
                });
    }

    private Future<Void> resolveDatasetTitle(Iterator<Object> iterator, String lang, Map<String, String> cache) {
        if (iterator.hasNext()) {
            JsonObject value = (JsonObject) iterator.next();
            String dataset = value.getString("reference");
            if (cache.containsKey(dataset)) {
                value.put("title", cache.get(dataset));
                return resolveDatasetTitle(iterator, lang, cache);
            } else {
                return getDatasetTitle(dataset, lang)
                        .onSuccess(title -> {
                            value.put("title", title);
                            cache.put(dataset, title);
                        })
                        .transform(ar -> resolveDatasetTitle(iterator, lang, cache));
            }
        } else {
            return Future.succeededFuture();
        }
    }

    private Future<String> getDatasetTitle(String uri, String prefLang) {
        String query = String.format(QueryCollection.getQuery("DatasetTitle"), uri, prefLang, prefLang);
        return tripleStore.select(query)
                .map(resultSet -> {
                    if (resultSet.hasNext()) {
                        QuerySolution solution = resultSet.next();
                        if (solution.contains("preferred")) {
                            return solution.getLiteral("preferred").getLexicalForm();
                        } else if (solution.contains("default")) {
                            return solution.getLiteral("default").getLexicalForm();
                        } else if (solution.contains("empty")) {
                            return solution.getLiteral("empty").getLexicalForm();
                        } else if (solution.contains("any")) {
                            return solution.getLiteral("any").getLexicalForm();
                        } else {
                            throw new ServiceException(500, "No title found");
                        }
                    } else {
                        throw new ServiceException(500, "No title found");
                    }
                });
    }

    private Future<JsonObject> getErrorStatusCodes(String catalogueId, int offset, int limit, String lang) {
        Promise<JsonObject> promise = Promise.promise();

        JsonObject result = new JsonObject();

        DCATAPUriRef uriRef = DCATAPUriSchema.createForCatalogue(catalogueId);
//        QueryHandler queryHandler = new StatusCodeErrorsQueryHandler(uriRef.getCatalogueGraphName(), uriRef.getCatalogueUriRef(), offset, limit);
//        queryHandler.query(tripleStore)
//        QueryHandler queryAccessUrlCountHandler = new StatusCodeErrorsCountQueryHandler(uriRef.getCatalogueGraphName(), uriRef.getCatalogueUriRef(), DCAT.accessURL, PV.accessUrlStatusCode);
//        queryAccessUrlCountHandler.query(tripleStore)
//                .compose(count -> {
//                    response.put("count", response.getInteger("count", 0) + count.getInteger("count", 0));
//                    QueryHandler queryDownloadUrlCountHandler = new StatusCodeErrorsCountQueryHandler(uriRef.getCatalogueGraphName(), uriRef.getCatalogueUriRef(), DCAT.downloadURL, PV.downloadUrlStatusCode);
//                    return queryDownloadUrlCountHandler.query(tripleStore);
//                })
        QueryHandler queryAccessUrlCountHandler = new StatusCodeErrorsCountQueryHandler(uriRef.getCatalogueUriRef());
        queryAccessUrlCountHandler.queryFrom(tripleStore)
                .compose(count -> {
                    result.put("count", result.getInteger("count", 0) + count.getInteger("count", 0));
                    if (offset == 0 && limit == 1) {
                        return Future.succeededFuture(result);
                    } else {
                        return new StatusCodeErrorsQueryHandler(uriRef.getCatalogueUriRef(), offset, limit)
                                .queryFrom(tripleStore);
                    }
                })
                .onSuccess(statusCodes -> {
                    Iterator<Object> it = statusCodes.getJsonArray("results", new JsonArray()).iterator();

                    resolveDatasetTitle(it, lang, new HashMap<>())
                            .onComplete(ar -> {
                                result.put("results", statusCodes.getJsonArray("results", new JsonArray()));
                                promise.complete(result);
                            });
                })
                .onFailure(promise::fail);

        return promise.future();
    }

    private Future<StatusCodes> getStatusCodes(Resource metric, DocumentScope documentScope, String id, Long totalCount) {
        return StatusCodeQueryHandler.create(documentScope, id, metric)
                .queryFrom(tripleStore)
                .map(result -> {
                    if (!result.isEmpty()) {
                        int sum = result.fieldNames().stream().mapToInt(result::getInteger).sum();

                        Map<String, Double> statusCodes = new HashMap<>();
                        result.fieldNames().forEach(name -> statusCodes.put(name, result.getDouble(name) / sum * 100));

                        return new StatusCodes(PercentageMath.roundMap(statusCodes));
                    } else {
                        return new StatusCodes(Collections.emptyMap());
                    }
                });
    }

    private Future<Double> getYesPercentage(Resource metric, DocumentScope scope, String id, Long totalCount) {
        return BooleanMeasurementsQueryHandler.create(scope, metric, id)
                .queryFrom(tripleStore)
                .map(result -> {
                    if (result.isEmpty()) {
                        return -1.0;
                    } else {
                        if (totalCount != -1L && result.containsKey("no")) {
                            result.put("no", totalCount - result.getInteger("yes", 0));
                        }
                        return PercentageMath.calculateYesPercentage(result);
                    }
                }).otherwise(-1.0);
    }

    private Future<List<String>> getCatalogueList() {
        return new CatalogueInfosQueryHandler()
                .queryFrom(tripleStore)
                .map(catalogues -> {
                    catalogueInfos.clear();
                    catalogues.forEach(entry -> catalogueInfos.put(entry.getKey(), (JsonObject) entry.getValue()));
                    return new ArrayList<>(catalogueInfos.keySet());
                });
    }

}