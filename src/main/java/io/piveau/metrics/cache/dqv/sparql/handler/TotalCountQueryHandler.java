
package io.piveau.metrics.cache.dqv.sparql.handler;

import io.piveau.dcatap.DCATAPUriRef;
import io.piveau.dcatap.DCATAPUriSchema;
import io.piveau.metrics.cache.dqv.sparql.util.SparqlHelper;
import io.piveau.metrics.cache.persistence.DocumentScope;
import org.apache.jena.query.ResultSet;

public class TotalCountQueryHandler extends QueryHandler {

    public TotalCountQueryHandler(String queryName, Object ... parameters) {
        super(queryName, parameters);
    }

    public static QueryHandler create(String id, DocumentScope scope) {
        switch (scope) {
            case CATALOGUE:
                DCATAPUriRef uriRef = DCATAPUriSchema.applyFor(id);
                return new TotalCountQueryHandler("CatalogueTotalCount", uriRef.getCatalogueUriRef());
            case COUNTRY:
                return new TotalCountQueryHandler("CountryTotalCount", SparqlHelper.getCountries().get(id));
            case GLOBAL:
                return new TotalCountQueryHandler("AllTotalCount");
            case SCORE:
            default:
                throw new IllegalArgumentException("Unknown scope '" + scope + "'");
        }
    }

    @Override
    public void handle(ResultSet resultSet) {
        if (resultSet.hasNext()) {
            result.put("count", resultSet.next().getLiteral("count").getInt());
        }
    }

}
