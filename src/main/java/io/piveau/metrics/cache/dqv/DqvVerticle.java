package io.piveau.metrics.cache.dqv;

import io.piveau.dcatap.TripleStore;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.serviceproxy.ServiceBinder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DqvVerticle extends AbstractVerticle {
    private static final Logger log = LoggerFactory.getLogger(DqvVerticle.class);

    @Override
    public void start(Promise<Void> startPromise) {
        log.info("Starting DQV service...");
        TripleStore tripleStore = new TripleStore(vertx, config());

        DqvProvider dqvProvider = DqvProvider.create(tripleStore);
        new ServiceBinder(vertx)
                .setAddress(DqvProvider.SERVICE_ADDRESS)
                .register(DqvProvider.class, dqvProvider);

        log.info("DQV service started");
        startPromise.complete();
    }

}
