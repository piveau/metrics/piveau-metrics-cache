package io.piveau.metrics.cache.dqv;

import io.piveau.dcatap.DCATAPUriSchema;
import io.piveau.dcatap.TripleStore;
import io.piveau.metrics.cache.dqv.sparql.QueryCollection;
import io.piveau.metrics.cache.dqv.sparql.util.PercentageMath;
import io.piveau.vocabularies.vocabulary.DQV;
import io.piveau.vocabularies.vocabulary.PV;
import io.piveau.vocabularies.vocabulary.SHACL;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.serviceproxy.ServiceException;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.rdf.model.*;
import org.apache.jena.vocabulary.DC_11;
import org.apache.jena.vocabulary.OA;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static io.piveau.metrics.cache.Constants.*;

public class DatasetMetrics {
    private static final String NO_TITLE = "No title available";
    private static final String COUNT = "count";
    private static final String ITEMS = "items";

    private static final String ERRORS = "errors";
    private static final String WARNINGS = "warnings";
    private static final String INFOS = "infos";

    private Resource dataset;

    private final Model model;
    private final ArrayList<Resource> distributions = new ArrayList<>();
    private final TripleStore tripleStore;
    private final String lang;

    // logger
    private static final Logger LOG = LoggerFactory.getLogger(DatasetMetrics.class);

    public DatasetMetrics(Model model, TripleStore tripleStore, String lang) {
        this.model = model;
        this.tripleStore = tripleStore;
        this.lang = lang;

        ResIterator resIterator = model.listResourcesWithProperty(DQV.hasQualityMeasurement);
        while (resIterator.hasNext()) {
            Resource resource = resIterator.next();
            if (resource.isURIResource()) {
                // check if resource is dataset or distribution
                if (DCATAPUriSchema.isDatasetUriRef(resource.getURI())) {
                    dataset = resource;
                } else if (DCATAPUriSchema.isDistributionUriRef(resource.getURI())) {
                    distributions.add(resource);
                }
            }
        }
    }

    /**
     * Extracts dataset and distribution resources and returns a JSON Object with all metrics data for a dataset
     */
    public JsonObject getDatasetMetrics() {
        if (dataset == null) {
            throw new ServiceException(404, "No dataset found");
        } else {
            return new JsonObject()
                    .put("success", true)
                    .put("result", new JsonObject()
                            .put(COUNT, 1)
                            .put("results", new JsonArray()
                                    .add(new JsonObject()
                                            .put("info", new JsonObject()
                                                    .put("dataset-id", dataset.getLocalName())
                                                    .put("dataset-uri", dataset.getURI())
//                                                        .put("distributions", distributionDetails)
                                                    .put("score", getScore()))
                                            .put(DIMENSION_ACCESSIBILITY, new JsonArray()
                                                    .add(calculateMetricsPercentageForDistribution(PV.downloadUrlAvailability))
                                                    .add(generateStatusCodePercentage(PV.accessUrlStatusCode))
                                                    .add(generateStatusCodePercentage(PV.downloadUrlStatusCode)))
                                            .put(DIMENSION_REUSABLITY, new JsonArray()
                                                    .add(extractMetric(dataset, PV.accessRightsAvailability))
                                                    .add(calculateMetricsPercentageForDistribution(PV.licenceAvailability))
                                                    .add(extractMetric(dataset, PV.accessRightsVocabularyAlignment))
                                                    .add(extractMetric(dataset, PV.contactPointAvailability))
                                                    .add(extractMetric(dataset, PV.publisherAvailability)))
                                            .put(DIMENSION_CONTEXTUALITY, new JsonArray()
                                                    .add(calculateMetricsPercentageForDistribution(PV.byteSizeAvailability))
                                                    .add(calculateMetricsPercentageForDistribution(PV.rightsAvailability))
                                                    // dateModifiedAvailability and dateIssuedAvailability need to be be added for both, the dataset itself and its distributions
                                                    .add(new JsonObject().put("dataset", new JsonArray()
                                                            .add(extractMetric(dataset, PV.dateModifiedAvailability))
                                                            .add(extractMetric(dataset, PV.dateIssuedAvailability))))
                                                    .add(new JsonObject().put("distributions", new JsonArray()
                                                            .add(calculateMetricsPercentageForDistribution(PV.dateModifiedAvailability))
                                                            .add(calculateMetricsPercentageForDistribution(PV.dateIssuedAvailability)))))
                                            .put(DIMENSION_FINDABILITY, new JsonArray()
                                                    .add(extractMetric(dataset, PV.keywordAvailability))
                                                    .add(extractMetric(dataset, PV.categoryAvailability))
                                                    .add(extractMetric(dataset, PV.spatialAvailability))
                                                    .add(extractMetric(dataset, PV.temporalAvailability)))
                                            .put(DIMENSION_INTEROPERABILITY, new JsonArray()
                                                    .add(extractMetric(dataset, PV.dcatApCompliance))
                                                    .add(calculateMetricsPercentageForDistribution(PV.formatAvailability))
                                                    .add(calculateMetricsPercentageForDistribution(PV.mediaTypeAvailability))
                                                    .add(calculateMetricsPercentageForDistribution(PV.formatMediaTypeVocabularyAlignment))))));
        }
    }

    /**
     * Extracts distribution resources and returns a JSON Object with all distributions and their metrics for one dataset
     */
    public Future<JsonObject> getDistributionMetricsPerDataset() {
        return createDistributionsJson();
    }

    /**
     * Generates a JSON Object from the metrics data for a dataset and its distributions
     */
    private Future<JsonObject> createDistributionsJson() {
        JsonArray distributionsData = new JsonArray();
        List<Future<String>> futures = distributions.stream()
                .map(distribution -> getDistributionTitle(distribution.getURI(), lang)
                        .onSuccess(title -> createDistributionInfo(distribution, distributionsData, title))
                        .onFailure(cause -> createDistributionInfo(distribution, distributionsData, NO_TITLE)))
                .toList();

        return Future.join(futures).map(cf ->
                new JsonObject()
                        .put("success", true)
                        .put("result", new JsonObject()
                                .put(COUNT, distributions.size())
                                .put("results", distributionsData))
        );
    }

    /**
     * Looks for a certain metric connected to a resource in the model and returns a JSON Object containing the metric name and its value
     *
     * @param resource Resource to extract the data from
     * @param metric   Metric to search for
     */
    private JsonObject extractMetric(Resource resource, Resource metric) {
        JsonObject output = new JsonObject().put(metric.getLocalName(), new JsonObject());

        model.listStatements(null, DQV.computedOn, resource)
                .mapWith(Statement::getSubject)
                .filterKeep(measurement -> measurement.hasProperty(DQV.isMeasurementOf, metric))
                .filterKeep(measurement -> measurement.hasProperty(DQV.value))
                .mapWith(measurement -> measurement.getProperty(DQV.value).getObject().asLiteral())
                .nextOptional()
                .ifPresent(value -> {
                    if (metric.equals(PV.accessUrlStatusCode) || metric.equals(PV.downloadUrlStatusCode)) {
                        output.put(metric.getLocalName(), value.getInt());
                    } else {
                        output.put(metric.getLocalName(), value.getBoolean());
                    }
                });

        return output;
    }

    /**
     * Calculates the yes and no percentages for one metric over several distributions
     *
     * @param metric Metric for which to calculate the percentages
     */
    private JsonObject calculateMetricsPercentageForDistribution(Resource metric) {
        Map<String, Long> frequency = distributions.stream()
                .map(distribution -> extractMetric(distribution, metric))
                .collect(Collectors.groupingBy(measurement ->
                        Boolean.TRUE.equals(measurement.getBoolean(metric.getLocalName())) ? "yes" : "no", Collectors.counting()));

        return new JsonObject().put(metric.getLocalName(), PercentageMath.getYesNoPercentage(frequency));
    }

    /**
     * Get the score for a dataset
     */
    private int getScore() {
        return model.listStatements(null, DQV.computedOn, dataset)
                .mapWith(Statement::getSubject)
                .filterKeep(measurement -> measurement.hasProperty(DQV.isMeasurementOf, PV.scoring))
                .filterKeep(measurement -> measurement.hasProperty(DQV.value))
                .mapWith(measurement -> measurement.getProperty(DQV.value).getInt())
                .nextOptional()
                .orElse(0);
    }

    /**
     * Generates the percentages for each status code of an access or download URL
     *
     * @param metric Metric for which to calculate the percentages
     */
    private JsonObject generateStatusCodePercentage(Resource metric) {
        List<Integer> statusCodes = model.listStatements(null, DQV.isMeasurementOf, metric)
                .mapWith(Statement::getSubject)
                .filterKeep(measurement -> measurement.hasProperty(DQV.value))
                .mapWith(measurement -> measurement.getProperty(DQV.value).getInt())
                .toList();

        Map<String, Double> percentages = statusCodes.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet()
                .stream()
                .map(entry -> Map.entry(entry.getKey(), Double.valueOf(entry.getValue()) / statusCodes.size() * 100))
                .collect(Collectors.toMap(e -> e.getKey().toString(), Map.Entry::getValue));

        return new JsonObject().put(metric.getLocalName(), new StatusCodes(PercentageMath.roundMap(percentages)).toJsonArray());
    }

    private JsonObject extractDistributionStatusCode(Resource distribution, Resource metric) {

        // collect all values of this metric type
        List<Integer> values = model.listStatements(distribution, DQV.hasQualityMeasurement, (RDFNode) null)
                .filterKeep(statement -> statement.getObject().isResource())
                .mapWith(statement -> statement.getObject().asResource())
                .filterKeep(measurement -> measurement.hasProperty(DQV.isMeasurementOf, metric))
                .filterKeep(measurement -> measurement.hasProperty(DQV.value))
                .mapWith(measurement -> measurement.getProperty(DQV.value).getInt())
                .toList();

        // find the most frequent value
        return values.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet()
                .stream()
                .max(Map.Entry.comparingByValue())
                .map(entry -> new JsonObject().put(metric.getLocalName(), entry.getKey()))
                .orElse(new JsonObject().put(metric.getLocalName(), new JsonObject()));
    }

    private JsonObject extractCsvValidationResults(Resource distribution) {

        JsonObject validationResults = new JsonObject();

        // shortcut
        if (!distribution.hasProperty(DQV.hasQualityAnnotation)) {
            return validationResults;
        }

        validationResults.put(ERRORS, new JsonObject().put(COUNT, 0).put(ITEMS, new JsonArray()));
        validationResults.put(WARNINGS, new JsonObject().put(COUNT, 0).put(ITEMS, new JsonArray()));
        validationResults.put(INFOS, new JsonObject().put(COUNT, 0).put(ITEMS, new JsonArray()));

        distribution.listProperties(DQV.hasQualityAnnotation)
                .mapWith(statement -> statement.getObject().asResource())
                .mapWith(annotation -> annotation.getPropertyResourceValue(OA.hasBody))
                .filterDrop(Objects::isNull)
                .forEach(body -> {
                    Resource annotationType = body.getPropertyResourceValue(RDF.type);
                    if (annotationType != null) {
                        switch (annotationType.getLocalName()) {
                            case "CsvIndicator" -> {
                                validationResults.put("passed", body.getProperty(PV.passed).getBoolean());
                                validationResults.put("rowCount", body.getProperty(PV.rowNumber).getLong());
                                if (body.hasProperty(RDFS.comment)) {
                                    try {
                                        Integer comment = body.getProperty(RDFS.comment).getInt();
                                        LOG.debug("Adding limit: {}", comment);
                                        validationResults.put("limit", comment);
                                    } catch (Exception e) {
                                        LOG.error("Could not get comment for indicator: {}|| {}", body, body.getProperty(PV.columnNumber).getObject());
                                    }
                                }
                            }
                            case "CsvError" -> createCsvIndicatorItem(body, validationResults.getJsonObject("ERRORS"));
                            case "CsvWarning" -> createCsvIndicatorItem(body, validationResults.getJsonObject("WARNINGS"));
                            case "CsvInfo" -> createCsvIndicatorItem(body, validationResults.getJsonObject("INFOS"));
                            default -> LOG.warn("Unknown annotation type: {}", annotationType);
                        }
                    }
                });

        validationResults.put("itemCount",
                validationResults.getJsonObject(ERRORS).getInteger(COUNT)
                        + validationResults.getJsonObject(WARNINGS).getInteger(COUNT)
                        + validationResults.getJsonObject(INFOS).getInteger(COUNT));

        return validationResults;

    }

    private void createCsvIndicatorItem(Resource body, JsonObject entries) {
        JsonObject infoItem = new JsonObject();

        // column
        if (body.hasProperty(PV.columnNumber)) {
            infoItem.put("column", body.getProperty(PV.columnNumber).getInt());
        }

        // row
        if (body.hasProperty(PV.rowNumber)) {
            infoItem.put("row", body.getProperty(PV.rowNumber).getInt());
        }

        //title
        if (body.hasProperty(DC_11.title)) {
            infoItem.put("title", body.getProperty(DC_11.title).getString());
        }

        //message
        if (body.hasProperty(SHACL.resultMessage)) {
            infoItem.put("message", body.getProperty(SHACL.resultMessage).getString());
        }

        //indicator
        if (body.hasProperty(DC_11.identifier)) {
            infoItem.put("indicator", body.getProperty(DC_11.identifier).getString());
        }

        entries.getJsonArray(ITEMS).add(infoItem);
        entries.put(COUNT, entries.getInteger(COUNT, 0) + 1);
    }

    private Future<String> getDistributionTitle(String uri, String prefLang) {
        String query = String.format(QueryCollection.getQuery("DistributionTitle"), uri, prefLang, prefLang);
        return tripleStore.select(query)
                .map(resultSet -> {
                    if (resultSet.hasNext()) {
                        QuerySolution solution = resultSet.next();
                        if (solution.contains("preferred")) {
                            return solution.getLiteral("preferred").getLexicalForm();
                        } else if (solution.contains("default")) {
                            return solution.getLiteral("default").getLexicalForm();
                        } else if (solution.contains("empty")) {
                            return solution.getLiteral("empty").getLexicalForm();
                        } else if (solution.contains("any")) {
                            return solution.getLiteral("any").getLexicalForm();
                        } else {
                            return NO_TITLE;
                        }
                    } else {
                        return NO_TITLE;
                    }
                });
    }

    private void createDistributionInfo(Resource distribution, JsonArray distributionData, String distributionTitle) {
        distributionData.add(new JsonArray()
                .add(new JsonObject()
                        .put("info", new JsonObject()
                                .put("distribution-id", DCATAPUriSchema.parseUriRef(distribution.getURI()).getId())
                                .put("distribution-uri", distribution.getURI())
                                .put("distribution-title", distributionTitle))
                        .put(DIMENSION_ACCESSIBILITY, new JsonArray()
                                .add(extractMetric(distribution, PV.downloadUrlAvailability))
                                .add(extractDistributionStatusCode(distribution, PV.accessUrlStatusCode))
                                .add(extractDistributionStatusCode(distribution, PV.downloadUrlStatusCode)))
                        .put(DIMENSION_REUSABLITY, new JsonArray()
                                .add(extractMetric(distribution, PV.licenceAvailability)))
                        .put(DIMENSION_CONTEXTUALITY, new JsonArray()
                                .add(extractMetric(distribution, PV.byteSizeAvailability))
                                .add(extractMetric(distribution, PV.rightsAvailability))
                                .add(extractMetric(distribution, PV.dateModifiedAvailability))
                                .add(extractMetric(distribution, PV.dateIssuedAvailability)))
                        .put(DIMENSION_INTEROPERABILITY, new JsonArray()
                                .add(extractMetric(distribution, PV.formatAvailability))
                                .add(extractMetric(distribution, PV.mediaTypeAvailability))
                                .add(extractMetric(distribution, PV.formatMediaTypeVocabularyAlignment)))
                        .put("validation", extractCsvValidationResults(distribution))));
    }

}