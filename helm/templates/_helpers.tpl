{{/*
Expand the name of the chart.
*/}}
{{- define "piveau-metrics-cache.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "piveau-metrics-cache.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "piveau-metrics-cache.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "piveau-metrics-cache.labels" -}}
helm.sh/chart: {{ include "piveau-metrics-cache.chart" . }}
{{ include "piveau-metrics-cache.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "piveau-metrics-cache.selectorLabels" -}}
app.kubernetes.io/name: {{ include "piveau-metrics-cache.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "piveau-metrics-cache.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "piveau-metrics-cache.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}

{{/*
Create imagePullCredentials
*/}}
{{- define "imagePullCredentials" -}}
{"auths":{
{{- range $index, $val := .Values.imagePullCredentials -}}
  "{{ $val.address }}":{"username":"{{ $val.username }}","password":"{{ $val.password }}","auth":"{{ (printf "%s:%s" $val.username $val.password) | b64enc }}","email":"{{ $val.email }}"}
  {{- if (lt $index (sub (len $.Values.imagePullCredentials) 1)) -}},{{- end -}}
{{- end -}}
}}
{{- end }}

{{/*
Create imagePullSecret
*/}}
{{- define "imagePullSecret" -}}
{{ (include "imagePullCredentials" .) | b64enc }}
{{- end }}


{{/*
Create a name for quickchart
*/}}
{{- define "piveau-metrics-cache.quickchartName" -}}
{{- $name := "quickchart" }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}

{{/*
quickchart common labels
*/}}
{{- define "piveau-metrics-cache.quickchartLabels" -}}
helm.sh/chart: {{ include "piveau-metrics-cache.chart" . }}
{{ include "piveau-metrics-cache.quickchartSelectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
quickchart selector labels
*/}}
{{- define "piveau-metrics-cache.quickchartSelectorLabels" -}}
app.kubernetes.io/name: "quickchart"
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}